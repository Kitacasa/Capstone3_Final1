import { useState, useEffect, useContext } from 'react';
import { Form, Button, Card, Row, Col } from 'react-bootstrap';
import { Link, Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function Login () {


    const { user, setUser } = useContext(UserContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    
    function loginUser(e){
        e.preventDefault();


        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            //console.log(data)
            console.log(data.access);

            if(data.access !== undefined){
                console.log(data);
                localStorage.setItem('access', data.access);
                retrieveUserDetails(data.access);
                console.log(setUser.id, setUser.isAdmin);

                Swal.fire({
                    title: "Login Successful!",
                    icon: "success",
                    text: "Welcome to JK's Cakes & Bakes"
                })    
            } 
            else {
                Swal.fire({
                    title: 'Aunthentication Failed',
                    icon: 'error',
                    text: 'Please check your login details'
                })
            }
        })
        setEmail('');
        setPassword('');
    }

    const retrieveUserDetails = (token) =>{

        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers:{
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
            

        })
    }

    useEffect (() => {
        if (email !== '' && password !== '') {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [email, password])

    return (
        (user.id !== null)?

        <Navigate to = '/' />
        :
    
    <div className='container'>  
    <Row className='justify-content-center'>
        <Col xs md='6'>
        <h2 className='text-center my-4'>Sign In</h2>
        <p className='text-center mb-4'>
            Not registered yet? <Link to='/register'>Sign Up</Link> 
        </p>
        
        <Form onSubmit = {(e) => loginUser(e)}>
            <Card.Body>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
                <Form.Text className= 'text-muted'>We'll never share your email with anyone else.</Form.Text>
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
            </Form.Group>
            </Card.Body>

            <div className='pt-3 text-center d-grid'>
            { isActive ?  // true
                <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                </Button>
                : // false
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
            }
            </div>
        </Form>
        
        
        </Col>
    </Row>
    </div>
    )

}


